#include <chrono>
#include <cmath>
#include <functional>
#include <iostream>
#include <unordered_map>
#include <vector>

#include <SFML/Graphics.hpp>

#include <ExpressionCompiler2/compiler.hpp>

#include "cartesianAxes.hpp"
#include "cartesianFunction.hpp"
#include "graphCanvas.hpp"

int main()
{
	std::cout<<"Enter an expression in terms of x and a (time)"<<std::endl;
	std::string expression;
	std::getline ( std::cin, expression );
	ec::Compiler compiler ( [] (std::string error) {
		std::cout<<error<<std::endl;
	} );

	GraphCanvas graphCanvas ( { 0, 0, 800, 800 }, { -5, 5, 10, -10 } );

	CartesianAxes axes ( -5, 5, -5, 5, 1, 1 );
	axes.setAxesColour ( { 0, 255, 255 } );
	axes.setGridColour ( { 64, 0, 128 } );
	graphCanvas.addCartesianAxes ( "mainAxes", axes );

	std::unique_ptr<Plotter> plotter = std::make_unique<Plotter> ( "x" );
	plotter->addCartesianFunction ( "func", compiler.compile ( expression ), CartesianFunction::PlottingMode::IndependentX );
	plotter->getFunction ( "func" ).setLowerBound ( -5.0 );
	plotter->getFunction ( "func" ).setUpperBound ( 5.0 );
	plotter->getFunction ( "func" ).setPointGap ( 0.02 );
	plotter->getFunction ( "func" ).setColour ( {255, 255, 255} );

	std::cout<<"Enter another expression in terms of x and a (time)"<<std::endl;
	std::getline ( std::cin, expression );

	plotter->addCartesianFunction ( "func2", compiler.compile ( expression ) , CartesianFunction::PlottingMode::IndependentX );
	plotter->getFunction ( "func2" ).setLowerBound ( -5.0 );
	plotter->getFunction ( "func2" ).setUpperBound ( 5.0 );
	plotter->getFunction ( "func2" ).setPointGap ( 0.02 );
	plotter->getFunction ( "func2" ).setColour ( {0,255,0} );

	graphCanvas.addPlotter ( "plotter", std::move ( plotter ) );

	sf::RenderWindow window ( sf::VideoMode ( 800, 800 ), "Graph canvas" );

	auto start = std::chrono::steady_clock::now();

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			}
		}

		graphCanvas.getPlotter ( "plotter" ).setVariable ( "a", (std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::steady_clock::now()-start)).count() );
		graphCanvas.update();

		window.clear();
		graphCanvas.preDraw();
		window.draw ( graphCanvas );
		window.display();
	}

	return 0;
}
