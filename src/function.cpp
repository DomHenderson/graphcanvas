//SFML GRAPHICS INCLUDES
#include <SFML/Graphics/Color.hpp>         //sf::Color
#include <SFML/Graphics/PrimitiveType.hpp> //sf::LineStrip
#include <SFML/Graphics/VertexArray.hpp>   //sf::VertexArray

//INTERNAL INCLUDES
#include "function.hpp" //gc::Function

namespace gc {

//Refill the vertex array with the correct number of points.
//This prevents it having to be done every frame.
void Function::RecreateVertexArray()
{
	double start ( m_Start );
	if ( m_HasLowerBound && m_LowerBound > start ) {
		start = m_LowerBound;
	}

	double end ( m_End );
	if ( m_HasUpperBound && m_UpperBound < end ) {
		end = m_UpperBound;
	}

	m_Vertices.clear();
	for ( double i ( start ); i <= end; i += m_Gap ) {
		m_Vertices.append ( { { 0, 0 }, m_Colour } );
	}
}

Function::Function() :
	m_Start ( 0.0 ),
	m_End ( 1.0 ),
	m_HasLowerBound ( false ),
	m_LowerBound ( 0.0 ),
	m_HasUpperBound ( false ),
	m_UpperBound ( 0.0 ),
	m_Gap ( 0.01 ),
	m_HasMinimumOut ( false ),
	m_MinimumOut ( 0.0 ),
	m_HasMaximumOut ( false ),
	m_MaximumOut ( 0.0 ),
	m_Colour ( sf::Color::White ),
	m_Vertices ( sf::LineStrip, 0 )
{
	RecreateVertexArray();
}

void Function::setColour( sf::Color colour )
{
	m_Colour = colour;
	RecreateVertexArray();
}

void Function::setVisibleDomain ( double start, double end )
{
	m_Start = start;
	m_End = end;
	RecreateVertexArray();
}

void Function::setLowerBound ( double lowerBound )
{
	m_LowerBound = lowerBound;
	m_HasLowerBound = true;
	RecreateVertexArray();
}

void Function::setUpperBound ( double upperBound )
{
	m_UpperBound = upperBound;
	m_HasUpperBound = true;
	RecreateVertexArray();
}

void Function::removeLowerBound ()
{
	m_HasLowerBound = false;
	RecreateVertexArray();
}

void Function::removeUpperBound ()
{
	m_HasUpperBound = false;
	RecreateVertexArray();
}

void Function::setMaximumOut( double maximumOut )
{
	m_MaximumOut = maximumOut;
	m_HasMaximumOut = true;
}

void Function::setMinimumOut ( double minimumOut )
{
	m_MinimumOut = minimumOut;
	m_HasMinimumOut = true;
}

void Function::removeMaximumOut ()
{
	m_HasMaximumOut = false;
}

void Function::removeMinimumOut ()
{
	m_HasMinimumOut = false;
}

void Function::setPointGap ( double gap )
{
	m_Gap = gap;
	RecreateVertexArray();
}

}
