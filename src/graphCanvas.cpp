//STANDARD LIBRARY INCLUDES
#include <memory>  //std::unique_ptr
#include <string>  //std::string
#include <utility> //std::move

//SFML SYSTEM INCLUDES
#include <SFML/System/Vector2.hpp> //sf::Vector2f

//SFML GRAPHICS INCLUDES
#include <SFML/Graphics/PrimitiveType.hpp> //sf::Quads
#include <SFML/Graphics/Rect.hpp>          //sf::FloatRect
#include <SFML/Graphics/RenderTexture.hpp> //sf::RenderTexture
#include <SFML/Graphics/Shader.hpp>        //sf::Shader
#include <SFML/Graphics/VertexArray.hpp>   //sf::VertexArray
#include <SFML/Graphics/View.hpp>          //sf::View

//INTERNAL INCLUDES
#include "cartesianAxes.hpp" //gc::CartesianAxes
#include "graphCanvas.hpp"   //gc::GraphCanvas
#include "plotter.hpp"       //gc::Plotter

namespace gc {

GraphCanvas::GraphCanvas( sf::FloatRect drawArea, sf::FloatRect graphArea ) :
	m_VertexArray ( sf::Quads, 4 ),
	m_DefaultView ( drawArea ), m_GraphView ( graphArea )
{
	//The vertex array needs to be set up as a rectangle coverring the entire draw area.
	//Draw area is the area on screen to which the result will be drawn. i.e. real coordinates
	//Graph area is the area of graph that is plotted. i.e. virtual coordinates
	m_VertexArray[0].position = { drawArea.left, drawArea.top };
	m_VertexArray[1].position = { drawArea.left + drawArea.width, drawArea.top };
	m_VertexArray[2].position = { drawArea.left + drawArea.width, drawArea.top + drawArea.height };
	m_VertexArray[3].position = { drawArea.left, drawArea.top + drawArea.height };

	m_VertexArray[0].texCoords = { 0, 0 };
	m_VertexArray[1].texCoords = { drawArea.width, 0 };
	m_VertexArray[2].texCoords = { drawArea.width, drawArea.height };
	m_VertexArray[3].texCoords = { 0, drawArea.height };

	//Lots of different RenderTextures are required for the FX system.
	m_PreFxTexture.create ( drawArea.width, drawArea.height );
	m_PreFxTexture.setView ( m_GraphView );
	m_ColourFxTexture[0].create ( drawArea.width, drawArea.height );
	m_ColourFxTexture[1].create ( drawArea.width, drawArea.height );
	m_GreyFxTexture[0].create ( drawArea.width, drawArea.height );
	m_GreyFxTexture[1].create ( drawArea.width, drawArea.height );
	m_PostFxTexture.create ( drawArea.width, drawArea.height );
	m_FinalTexture.create ( drawArea.width, drawArea.height );

	m_PreFxSprite.setTexture ( m_PreFxTexture.getTexture() );
	m_ColourSprite.setTexture ( m_ColourFxTexture[1].getTexture() );
	m_ColourFxSprite.setTexture ( m_ColourFxTexture[0].getTexture() );
	m_GreySprite.setTexture ( m_GreyFxTexture[1].getTexture() );
	m_GreyFxSprite.setTexture ( m_GreyFxTexture[0].getTexture() );
	m_PostFxSprite.setTexture ( m_PostFxTexture.getTexture() );

	//Shaders are used to provide cool blur effects to make the output more aesthetically pleasing.
	//This one blurs the image via a gaussian blur. This uses weights based on a normal distribution to produce a smooth effect.
	const std::string colourFragmentShader = \
	//The original pixels
	"uniform sampler2D source;" \
	//The shader is done in one dimension at a time.
	//This uniform is used to determine what direction it goes in.
	"uniform vec2 offsetFactor;" \

	"void main()" \
	"{" \
	"	const int sampleCount = 17;" \

	//Weights that very closely approximate the normal distribution
	"	float weights[sampleCount];" \
	"	weights[0] = 1.0 / 65536.0;" \
	"	weights[1] = 16.0 / 65536.0;" \
	"	weights[2] = 128.0 / 65536.0;" \
	"	weights[3] = 560.0 / 65536.0;" \
	"	weights[4] = 1820.0 / 65536.0;" \
	"	weights[5] = 4368.0 / 65536.0;" \
	"	weights[6] = 8008.0 / 65536.0;" \
	"	weights[7] = 11440.0 / 65536.0;" \
	"	weights[8] = 12870.0 / 65536.0;" \
	"	weights[9] = 11440.0 / 65536.0;" \
	"	weights[10] = 8008.0 / 65536.0;" \
	"	weights[11] = 4368.0 / 65536.0;" \
	"	weights[12] = 1820.0 / 65536.0;" \
	"	weights[13] = 560.0 / 65536.0;" \
	"	weights[14] = 128.0 / 65536.0;" \
	"	weights[15] = 16.0 / 65536.0;" \
	"	weights[16] = 1.0 / 65536.0;" \

	//The position of the current pixel
	"	vec2 textureCoordinates = gl_TexCoord[0].xy;" \
	//The new colour for the pixel
	"	vec4 colour = vec4(0.0);" \
	//Used to keep track of all the alpha modified weights, so that it can be used for averaging.
	"	float fullWeight = 0.0;" \

	"	for ( int i = 0; i < sampleCount; ++i ) {" \
			//The range is (sampleCount-1)/2 to either side of textureCoordinates, so it must use i-8 rather than i, so that it's centred.
	"		vec4 textureColour = texture2D ( source, textureCoordinates + (i-8) * offsetFactor );" \
			//This takes the effect of the alpha into account.
	"		float actualWeight = weights[i] * textureColour.a;" \
	"		colour.rgb += textureColour.rgb * actualWeight;" \
	"		colour.a += textureColour.a * weights[i];" \
	"		fullWeight += actualWeight;" \
	"	}" \
		//Weighted average = weighted sum / total of weights.
	"	colour.rgb /= fullWeight;" \
		//Modify the result so that a single opaque pixel surrounded by transparent ones will stay opaque.
	"	colour.a /= 12870.0 / 65536.0;" \
		//Apply the result of the shader.
	"	gl_FragColor = colour;" \
	"}";

	//Pass the shader code to SFML so that it can be used.
	m_ColourShader.loadFromMemory ( colourFragmentShader, sf::Shader::Fragment );
	m_ColourShader.setUniform ( "source", sf::Shader::CurrentTexture );

	//This shader adds a glow to the middle of the lines.
	//It does this by making a greyscale blur that is narrower than the full colour one and gets layered on top.
	const std::string greyFragmentShader = \
	"uniform sampler2D source;" \
	"uniform vec2 offsetFactor;" \
	"void main()" \
	"{" \
	"	vec2 textureCoordinates = gl_TexCoord[0].xy;" \
	"	vec4 colour = vec4(0.0);" \
	"	colour += texture2D(source, textureCoordinates - 2.0 * offsetFactor) * 8008.0 / 65536.0;" \
	"	colour += texture2D(source, textureCoordinates - offsetFactor) * 11440.0 / 65536.0;" \
	"	colour += texture2D(source, textureCoordinates) * 12870.0 / 65536.0;" \
	"	colour += texture2D(source, textureCoordinates + offsetFactor) * 11440.0 / 65536.0;" \
	"	colour += texture2D(source, textureCoordinates + 2.0 * offsetFactor) * 8008.0 / 65536.0;" \
	"	colour.w *= 5.0;" \
	"	gl_FragColor = vec4((colour.x+colour.y+colour.z)/3.0,(colour.x+colour.y+colour.z)/3.0,(colour.x+colour.y+colour.z)/3.0,colour.w) * 2.0;" \
	"}";

	m_GreyShader.loadFromMemory ( greyFragmentShader, sf::Shader::Fragment );
	m_GreyShader.setUniform ( "source", sf::Shader::CurrentTexture );
}

void GraphCanvas::addCartesianAxes ( std::string name, CartesianAxes axes )
{
	m_Axes [ name ] = axes;
}

CartesianAxes& GraphCanvas::getCartesianAxes ( std::string name )
{
	return m_Axes [ name ];
}

void GraphCanvas::removeCartesianAxes ( std::string name )
{
	m_Axes.erase ( name );
}

void GraphCanvas::addPlotter ( std::string name, std::unique_ptr<Plotter> plotter )
{
	m_Plotters [ name ] = std::move ( plotter );
}

Plotter& GraphCanvas::getPlotter ( std::string name )
{
	return *(m_Plotters [ name ]);
}

void GraphCanvas::removePlotter ( std::string name )
{
	m_Plotters.erase ( name );
}

void GraphCanvas::setDrawArea ( sf::FloatRect drawArea )
{
	m_DefaultView = sf::View ( drawArea );
	m_VertexArray[0].position = { drawArea.left, drawArea.top };
	m_VertexArray[1].position = { drawArea.left + drawArea.width, drawArea.top };
	m_VertexArray[2].position = { drawArea.left + drawArea.width, drawArea.top + drawArea.height };
	m_VertexArray[3].position = { drawArea.left, drawArea.top + drawArea.height };

	m_VertexArray[0].texCoords = { 0, 0 };
	m_VertexArray[1].texCoords = { drawArea.width, 0 };
	m_VertexArray[2].texCoords = { drawArea.width, drawArea.height };
	m_VertexArray[3].texCoords = { 0, drawArea.height };

	m_PreFxTexture.create ( drawArea.width, drawArea.height );
	m_PreFxTexture.setView ( m_GraphView );
	m_ColourFxTexture[0].create ( drawArea.width, drawArea.height );
	m_ColourFxTexture[1].create ( drawArea.width, drawArea.height );
	m_GreyFxTexture[0].create ( drawArea.width, drawArea.height );
	m_GreyFxTexture[1].create ( drawArea.width, drawArea.height );
	m_PostFxTexture.create ( drawArea.width, drawArea.height );
	m_FinalTexture.create ( drawArea.width, drawArea.height );

	//The boolean parameter is whether or not the textureRect should be reset.
	//Not doing this would cause issues if the texture has changed size.
	//If it got bigger, only a portion would be used, and if it got smaller, values from outside the texture would be used.
	m_PreFxSprite.setTexture ( m_PreFxTexture.getTexture(), true );
	m_ColourSprite.setTexture ( m_ColourFxTexture[1].getTexture(), true );
	m_ColourFxSprite.setTexture ( m_ColourFxTexture[0].getTexture(), true );
	m_GreySprite.setTexture ( m_GreyFxTexture[1].getTexture(), true );
	m_GreyFxSprite.setTexture ( m_GreyFxTexture[0].getTexture(), true );
	m_PostFxSprite.setTexture ( m_PostFxTexture.getTexture(), true );
}

void GraphCanvas::setGraphArea ( sf::FloatRect graphArea )
{
	m_GraphView = sf::View ( graphArea );
	m_PreFxTexture.setView ( m_GraphView );

	for ( auto& x: m_Plotters ) {
		x.second->setVisibleDomain ( graphArea.left, graphArea.left + graphArea.width, graphArea.top + graphArea.height, graphArea.top );
	}
}

sf::FloatRect GraphCanvas::getGraphArea () const
{
	return sf::FloatRect (
		{
			m_GraphView.getCenter().x - m_GraphView.getSize().x/2,
			m_GraphView.getCenter().y - m_GraphView.getSize().y/2
		},
		m_GraphView.getSize()
	);
}

//Create the image that will be drawn to the screen, but don't draw it yet.
void GraphCanvas::preDraw ()
{
	//All textures are set to transparent so that the host program can decide the background.
	//Having the host pass a colour instead would remove control, as this way, the background can be anything, not just a colour.
	//The main axes and the graphs are drawn to this texture, because they are the ones to which the glow effect will be applied.
	m_PreFxTexture.clear(sf::Color::Transparent);
	for ( auto& x: m_Axes ) {
		x.second.drawMainAxes( m_PreFxTexture );
	}
	for ( auto& x: m_Plotters ) {
		x.second->draw ( m_PreFxTexture );
	}
	m_PreFxTexture.display();

	//The first effect is the full colour blur.
	//This has to be done twice, as each pass blurs in one direction.
	m_ColourFxTexture[0].clear ( sf::Color::Transparent );
	//OpenGl uses normalised coordinates, not pixel coordinates, so this ensures that it acts upon each pixel individually.
	m_ColourShader.setUniform ( "offsetFactor", sf::Vector2f(1.f/m_DefaultView.getSize().x,0.f));
	m_ColourFxTexture[0].draw ( m_PreFxSprite, &m_ColourShader );
	m_ColourFxTexture[0].display();

	m_ColourFxTexture[1].clear ( sf::Color::Transparent);
	m_ColourShader.setUniform ( "offsetFactor", sf::Vector2f(0.f,1.f/m_DefaultView.getSize().y));
	m_ColourFxTexture[1].draw ( m_ColourFxSprite, &m_ColourShader );
	m_ColourFxTexture[1].display();

	//This uses the preFx texture again, but this time does a smaller greyscale blur.
	m_GreyFxTexture[0].clear ( sf::Color::Transparent );
	m_GreyShader.setUniform ( "offsetFactor", sf::Vector2f(1.f/m_DefaultView.getSize().x,0.f));
	m_GreyFxTexture[0].draw ( m_PreFxSprite, &m_GreyShader );
	m_GreyFxTexture[0].display();

	m_GreyFxTexture[1].clear(sf::Color::Transparent);
	m_GreyShader.setUniform ( "offsetFactor", sf::Vector2f(0.f,1.f/m_DefaultView.getSize().y));
	m_GreyFxTexture[1].draw ( m_GreyFxSprite, &m_GreyShader );
	m_GreyFxTexture[1].display();

	//The colour and greyscale versions are drawn on top of each other using an alpha scaled additive blend mode to ensure that they
	//both can be seen.
	m_PostFxTexture.clear ( sf::Color::Transparent );
	m_PostFxTexture.draw ( m_ColourSprite );
	m_PostFxTexture.draw ( m_GreySprite, sf::RenderStates ( sf::BlendMode ( sf::BlendMode::SrcAlpha, sf::BlendMode::DstAlpha, sf::BlendMode::Add ) ) );
	m_PostFxTexture.display();

	//The grid goes in the background, so it goes first.
	m_FinalTexture.clear ( sf::Color::Transparent );
	m_FinalTexture.setView ( m_GraphView );
	for ( auto& x: m_Axes ) {
		x.second.drawGrid ( m_FinalTexture );
	}
	m_FinalTexture.setView ( m_DefaultView );
	m_FinalTexture.draw ( m_PostFxSprite, sf::RenderStates ( sf::BlendMode ( sf::BlendMode::SrcAlpha, sf::BlendMode::DstAlpha, sf::BlendMode::Add ) ) );
	m_FinalTexture.display();
}

void GraphCanvas::draw ( sf::RenderTarget& target, sf::RenderStates states ) const
{
	//Combining these two transforms allows an object hierarchy where transforming parents also transforms their children.
	states.transform *= getTransform();

	states.texture = &(m_FinalTexture.getTexture());

	target.draw ( m_VertexArray, states );
}

void GraphCanvas::update()
{
	for ( auto& x: m_Plotters ) {
		x.second->update();
	}
}

}
