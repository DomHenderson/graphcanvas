//STANDARD LIBRARY INCLUDES
#include <cmath>         //std::sqrt
#include <functional>    //std::function
#include <memory>        //std::make_unique
#include <string>        //std::string
#include <unordered_map> //std::unordered_map

//SFML GRAPHICS INCLUDES
#include <SFML/Graphics/RenderTarget.hpp> //sf::RenderTarget

//INTERNAL INCLUDES
#include "cartesianFunction.hpp" //gc::CartesianFunction
#include "plotter.hpp"           //gc::Plotter
#include "polarFunction.hpp"     //gc::PolarFunction

namespace gc {

namespace {
	double MaxModulus ( double left, double right ) {
		if ( left < 0 ) {
			left *= -1;
		}
		if ( right < 0 ) {
			right *= -1;
		}
		if ( left > right ) {
			return left;
		} else {
			return right;
		}
	}
}

Plotter::Plotter ( std::string var ) :
	m_Var ( var ) //The variable which is plotted on the independent axis
	              //This has to be set, otherwise one function could be plotted with x on the x axis, and another could be plotted
	              //with x as a slider, which would interfere with the first.
{
}

Function& Plotter::getFunction ( std::string name )
{
	//Functions are held polymorphically by pointers, so to get a polymorphic reference, they must be dereferenced.
	return *(m_Functions [ name ]);
}

//There must be separate functions because the PlottingMode enums are defined separately from each other.
void Plotter::addCartesianFunction ( std::string name, std::function<double(std::unordered_map<std::string,double>&)> func, CartesianFunction::PlottingMode mode )
{
	m_Functions.emplace (
		name,
		std::make_unique<CartesianFunction> ( [ func, this ] ( double var ) {
			this->m_Parameters [ this->m_Var ] = var;
			return func ( this->m_Parameters );
		},
		mode
	) );

	m_DomainUpdateQueue.emplace (
		name,
		[ name, mode, this ] ( double xStart, double xEnd, double yStart, double yEnd ) {
			if ( mode == CartesianFunction::PlottingMode::IndependentX ) {
				m_Functions [ name ]->setVisibleDomain ( xStart, xEnd );
			} else {
				m_Functions [ name ]->setVisibleDomain ( yStart, yEnd );
			}
		}
	);
}

void Plotter::addPolarFunction ( std::string name, std::function<double(std::unordered_map<std::string,double>&)> func, PolarFunction::PlottingMode mode )
{
	m_Functions.emplace (
		name,
		std::make_unique<PolarFunction> ( [ func, this ] ( double var ) {
			this->m_Parameters [ this->m_Var ] = var;
			return func ( this->m_Parameters );
		},
		mode
	) );

	m_DomainUpdateQueue.emplace (
		name,
		[ name, mode, this ] ( double xStart, double xEnd, double yStart, double yEnd ) {
			if ( mode == PolarFunction::PlottingMode::IndependentT ) {
				m_Functions [ name ]->setVisibleDomain ( -3.14159265359, 3.14159265359 );
			} else {
				double maxX = MaxModulus ( xStart, xEnd );
				double maxY = MaxModulus ( yStart, yEnd );
				double distance = std::sqrt ( maxX * maxX + maxY * maxY );
				m_Functions [ name ]->setVisibleDomain ( -distance, distance );
			}
		}
	);
}

std::string Plotter::getIndependentVariable() const
{
	return m_Var;
}

void Plotter::removeFunction ( std::string name )
{
	m_Functions.erase ( name );

	m_DomainUpdateQueue.erase ( name );
}

void Plotter::setVariable ( std::string name, double value )
{
	m_Parameters [ name ] = value;
}

void Plotter::setVisibleDomain ( double xStart, double xEnd, double yStart, double yEnd ) {
	for ( auto& x: m_DomainUpdateQueue ) {
		x.second ( xStart, xEnd, yStart, yEnd );
	}
}

void Plotter::draw ( sf::RenderTarget& target )
{
	//The plotter itself is not transformable, which is why it does not need to recieve a RenderStates variable like most other drawable classes.
	for ( auto& x: m_Functions ) {
		x.second->draw ( target );
	}
}

void Plotter::update ()
{
	for ( auto& x: m_Functions ) {
		x.second->update();
	};
}

}
