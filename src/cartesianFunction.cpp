//STANDARD LIBRARY INCLUDES
#include <functional> //std::function

//SFML GRAPHICS INCLUDES
#include <SFML/Graphics/BlendMode.hpp>    //sf::BlendMode
#include <SFML/Graphics/RenderStates.hpp> //sf::RenderStates
#include <SFML/Graphics/RenderTarget.hpp> //sf::RenderTarget

//INTERNAL INCLUDES
#include "cartesianFunction.hpp" //gc::CartesianFunction

namespace gc {

CartesianFunction::CartesianFunction ( std::function<double(double)> function, PlottingMode mode ):
	m_Function ( function ),
	m_PlottingMode ( mode )
{
}

void CartesianFunction::draw ( sf::RenderTarget& target )
{
	target.draw ( m_Vertices, sf::RenderStates ( sf::BlendMode (sf::BlendMode::SrcColor, sf::BlendMode::DstColor, sf::BlendMode::Add ) ) );
}

void CartesianFunction::update ()
{
	//Start and end points have to be decided from either the edges of the visible range, or the held bounds.
	double start ( m_Start );
	if ( m_HasLowerBound && m_LowerBound > start ) {
		start = m_LowerBound;
	}

	double end ( m_End );
	if ( m_HasUpperBound && m_UpperBound < end ) {
		end = m_UpperBound;
	}

	if ( m_PlottingMode == PlottingMode::IndependentX ) {
		for ( unsigned i ( 0 ); start + i*m_Gap <= end; ++i ) {
			m_Vertices[i].position.x = start + i*m_Gap;
			m_Vertices[i].position.y = m_Function( start + i*m_Gap );
			m_Vertices[i].color.a = 255;

			//Points are made transparent if they excede the held minimum and maximum.
			if (
				( m_HasMinimumOut && m_Function ( start + i*m_Gap ) < m_MinimumOut ) ||
				( m_HasMaximumOut && m_Function ( start + i*m_Gap ) > m_MaximumOut )
			) {
				m_Vertices[i].color.a = 0;
			}
		}
	} else if ( m_PlottingMode == PlottingMode::IndependentY ) {
		for ( unsigned i ( 0 ); start + i*m_Gap <= end; ++i ) {
			m_Vertices[i].position.y = start + i*m_Gap;
			m_Vertices[i].position.x = m_Function( start + i*m_Gap );
			m_Vertices[i].color.a = 255;

			if (
				( m_HasMinimumOut && m_Function ( start + i*m_Gap ) < m_MinimumOut ) ||
				( m_HasMaximumOut && m_Function ( start + i*m_Gap ) > m_MaximumOut )
			) {
				m_Vertices[i].color.a = 0;
			}
		}
	}
}

}
