//STANDARD LIBRARY INCLUDES
#include <cmath>      //std::cos, std::sin
#include <functional> //std::function

//SFML GRAPHICS INCLUDES
#include <SFML/Graphics/BlendMode.hpp>    //sf::BlendMode
#include <SFML/Graphics/Color.hpp>        //sf::Color
#include <SFML/Graphics/RenderStates.hpp> //sf::RenderStates
#include <SFML/Graphics/RenderTarget.hpp> //sf::RenderTarget
#include <SFML/Graphics/VertexArray.hpp>  //sf::VertexArray

//INTERNAL INCLUDES
#include "polarFunction.hpp" //gc::PolarFunction

namespace gc {

PolarFunction::PolarFunction ( std::function<double(double)> function, PlottingMode mode ) :
	m_Function ( function ),
	m_PlottingMode ( mode )
{
	//All polar functions go between pi and -pi, unless the domain is restricted further.
	m_Start = -3.14159;
	m_End = 3.14159;
}

void PolarFunction::draw ( sf::RenderTarget& target )
{
	target.draw ( m_Vertices, sf::RenderStates ( sf::BlendMode (sf::BlendMode::SrcColor, sf::BlendMode::DstColor, sf::BlendMode::Add ) ) );
}

//This function is almost exactly the same as in CartesianFunction.
//The only difference is that because vertex coordinates still have to be cartesian, a conversion from polar to cartesian is needed.
void PolarFunction::update ()
{
	double start ( m_Start );
	if ( m_HasLowerBound && m_LowerBound > start ) {
		start = m_LowerBound;
	}

	double end ( m_End );
	if ( m_HasUpperBound && m_UpperBound < end ) {
		end = m_UpperBound;
	}

	if ( m_PlottingMode == PlottingMode::IndependentT ) {
		for ( unsigned i (0); start + i*m_Gap <= end; ++i ) {
			m_Vertices[i].position.x = std::cos( start + i*m_Gap ) * m_Function ( start + i*m_Gap );
			m_Vertices[i].position.y = std::sin( start + i*m_Gap ) * m_Function ( start + i*m_Gap );
			m_Vertices[i].color = m_Colour;
			if (
				( m_HasMinimumOut && m_Function ( start + i*m_Gap ) < m_MinimumOut ) ||
				( m_HasMaximumOut && m_Function ( start + i*m_Gap ) > m_MaximumOut )
			) {
				m_Vertices[i].color = {0,0,0,0};
			}
		}
	} else if ( m_PlottingMode == PlottingMode::IndependentR ) {
		for ( unsigned i (0); start + i*m_Gap <= end; ++i ) {
			m_Vertices[i].position.x = ( start + i*m_Gap ) * std::cos ( m_Function ( start + i*m_Gap ) );
			m_Vertices[i].position.y = ( start + i*m_Gap ) * std::sin ( m_Function ( start + i*m_Gap ) );
			m_Vertices[i].color = m_Colour;
			if (
				( m_HasMinimumOut && m_Function ( start + i*m_Gap ) < m_MinimumOut ) ||
				( m_HasMaximumOut && m_Function ( start + i*m_Gap ) > m_MaximumOut )
			) {
				m_Vertices[i].color = {0,0,0,0};
			}
		}
	}
}

}
