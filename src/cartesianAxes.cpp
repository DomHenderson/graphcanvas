//STANDARD LIBRARY INCLUDES
#include <cmath> //std::fmod

//SFML GRAPHICS INCLUDES
#include <SFML/Graphics/BlendMode.hpp>     //sf::BlendMode
#include <SFML/Graphics/Color.hpp>         //sf::Color
#include <SFML/Graphics/PrimitiveType.hpp> //sf::Lines
#include <SFML/Graphics/RenderStates.hpp>  //sf::RenderStates
#include <SFML/Graphics/RenderTarget.hpp>  //sf::RenderTarget
#include <SFML/Graphics/VertexArray.hpp>   //sf::VertexArray

//INTERNAL INCLUDES
#include "cartesianAxes.hpp" //gc::CartesianAxes

namespace gc {

CartesianAxes::CartesianAxes ( float lowerXBound, float upperXBound, float lowerYBound, float upperYBound, float xMarkSpacing, float yMarkSpacing ):
	m_LowerXBound ( lowerXBound ), m_LowerYBound ( lowerYBound ), m_UpperXBound ( upperXBound ), m_UpperYBound ( upperYBound ),
	m_XMarkSpacing ( xMarkSpacing ), m_YMarkSpacing ( yMarkSpacing ),
	m_AxesColour ( 255, 255, 255 ), m_GridColour ( 64, 64, 64 ),
	m_MainAxes ( sf::Lines, 0 ), m_Grid ( sf::Lines, 0 )
{
	//It's easier to have this as a function that can be called again when properties are changed, because it avoids having to keep
	//track of what's where in the vertex arrays, and also because it has next to no effect on the performance of the program
	Recreate();
}

void CartesianAxes::Recreate()
{
	//Get rid of everything so that it can be remade.
	//This has to be done because the number of points is chosen dynamically.
	m_MainAxes.clear();
	m_Grid.clear();

	//These points form the main horizontal and vertical axes.
	m_MainAxes.append ( { { m_LowerXBound, 0.f }, m_AxesColour } );
	m_MainAxes.append ( { { m_UpperXBound, 0.f }, m_AxesColour } );
	m_MainAxes.append ( { { 0.f, m_LowerYBound }, m_AxesColour } );
	m_MainAxes.append ( { { 0.f, m_UpperYBound }, m_AxesColour } );

	//Find the position of the first tick on the x axis
	float xPos ( m_LowerXBound - std::fmod ( m_LowerXBound, m_XMarkSpacing ) );

	//Create a new one up until the end of the line
	while ( xPos <= m_UpperXBound ) {
		m_MainAxes.append ( { { xPos, 0.01f * ( m_UpperYBound - m_LowerYBound ) }, m_AxesColour } );
		m_MainAxes.append ( { { xPos, -0.01f * ( m_UpperYBound - m_LowerYBound ) }, m_AxesColour } );
		m_Grid.append ( { { xPos, m_UpperYBound }, m_GridColour } );
		m_Grid.append ( { { xPos, m_LowerYBound }, m_GridColour } );
		xPos += m_XMarkSpacing;
	}

	//Do the same for the y axis
	float yPos ( m_LowerYBound - std::fmod ( m_LowerYBound, m_YMarkSpacing ) );

	while ( yPos <= m_UpperYBound ) {
		m_MainAxes.append ( { { 0.01f * ( m_UpperXBound - m_LowerXBound ), yPos }, m_AxesColour } );
		m_MainAxes.append ( { { -0.01f * ( m_UpperXBound - m_LowerXBound ), yPos }, m_AxesColour } );
		m_Grid.append ( { { m_UpperXBound, yPos }, m_GridColour } );
		m_Grid.append ( { { m_LowerXBound, yPos }, m_GridColour } );
		yPos += m_YMarkSpacing;
	}
}

//DrawGrid and drawMainAxes are separate because the grid has to be drawn to a separate RenderTexture than main axes.
//This is because the blur effects only need to be applied to the main axes
void CartesianAxes::drawGrid ( sf::RenderTarget& target )
{
	target.draw ( m_Grid, sf::RenderStates ( sf::BlendMode (sf::BlendMode::SrcColor, sf::BlendMode::DstColor, sf::BlendMode::Add ) ) );
}

void CartesianAxes::drawMainAxes ( sf::RenderTarget& target )
{
	target.draw ( m_MainAxes, sf::RenderStates ( sf::BlendMode (sf::BlendMode::SrcColor, sf::BlendMode::DstColor, sf::BlendMode::Add ) ) );
}

void CartesianAxes::setAxesColour ( sf::Color colour )
{
	m_AxesColour = colour;
	Recreate();
}

void CartesianAxes::setGridColour ( sf::Color colour )
{
	m_GridColour = colour;
	Recreate();
}

void CartesianAxes::setLowerXBound( float lowerXBound )
{
	m_LowerXBound = lowerXBound;
	Recreate();
}

void CartesianAxes::setLowerYBound ( float lowerYBound )
{
	m_LowerYBound = lowerYBound;
	Recreate();
}

void CartesianAxes::setUpperXBound ( float upperXBound )
{
	m_UpperXBound = upperXBound;
	Recreate();
}

void CartesianAxes::setUpperYBound ( float upperYBound )
{
	m_UpperYBound = upperYBound;
	Recreate();
}

void CartesianAxes::setXMarkSpacing ( float xMarkSpacing )
{
	m_XMarkSpacing = xMarkSpacing;
	Recreate();
}

void CartesianAxes::setYMarkSpacing ( float yMarkSpacing )
{
	m_YMarkSpacing = yMarkSpacing;
	Recreate();
}

}
