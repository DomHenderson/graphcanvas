#ifndef POLARFUNCTION_HPP_INCLUDED
#define POLARFUNCTION_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <functional> //std::function

//SFML GRAHPICS INCLUDES
#include <SFML/Graphics/RenderTarget.hpp> //sf::RenderTarget

//INTERNAL INCLUDES
#include "function.hpp" //gc::Function

namespace gc {

class PolarFunction : public Function {
public:
	enum PlottingMode {
		IndependentR,
		IndependentT
	};

	PolarFunction() = default;
	PolarFunction ( std::function<double(double)> function, PlottingMode mode );
	virtual void draw ( sf::RenderTarget& target ) override;
	virtual void update () override;
private:
	std::function<double(double)> m_Function;

	PlottingMode m_PlottingMode;
};

}

#endif // POLARFUNCTION_HPP_INCLUDED
