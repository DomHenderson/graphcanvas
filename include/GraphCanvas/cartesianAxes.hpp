#ifndef CARTESIANAXES_HPP_INCLUDED
#define CARTESIANAXES_HPP_INCLUDED

//SFML GRAPHICS INCULDES
#include <SFML/Graphics/Color.hpp>        //sf::Color
#include <SFML/Graphics/RenderTarget.hpp> //sf::RenderTarget
#include <SFML/Graphics/VertexArray.hpp>  //sf::VertexArray

namespace gc {

class CartesianAxes {
public:
	CartesianAxes() = default;
	CartesianAxes( float lowerXBound, float upperXBound, float lowerYBound, float upperYBound, float xMarkSpacing, float yMarkSpacing );
	void drawGrid ( sf::RenderTarget& target );
	void drawMainAxes ( sf::RenderTarget& target );
	void setAxesColour ( sf::Color colour );
	void setGridColour ( sf::Color colour );
	void setLowerXBound ( float lowerXBound );
	void setLowerYBound ( float lowerYBound );
	void setUpperXBound ( float upperXBound );
	void setUpperYBound ( float upperYBound );
	void setXMarkSpacing ( float xMarkSpacing );
	void setYMarkSpacing ( float yMarkSpacing );
private:
	void Recreate();

	float m_LowerXBound;
	float m_LowerYBound;
	float m_UpperXBound;
	float m_UpperYBound;
	float m_XMarkSpacing;
	float m_YMarkSpacing;


	sf::Color m_AxesColour;
	sf::Color m_GridColour;

	sf::VertexArray m_MainAxes;
	sf::VertexArray m_Grid;
};

}

#endif // CARTESIANAXES_HPP_INCLUDED
