#ifndef CARTESIANFUNCTION_HPP_INCLUDED
#define CARTESIANFUNCTION_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <functional> //std::function

//SFML GRAPHICS INCLUDES
#include <SFML/Graphics/RenderTarget.hpp> //sf::RenderTarget

//INTERNAL INCLUDES
#include "function.hpp" //gc::Function

namespace gc {

class CartesianFunction : public Function {
public:
	enum class PlottingMode {
		IndependentX,
		IndependentY
	};

	CartesianFunction() = default;
	CartesianFunction ( std::function<double(double)> function, PlottingMode mode );
	virtual void draw ( sf::RenderTarget& target ) override;
	virtual void update () override;
private:
	std::function<double(double)> m_Function;

	PlottingMode m_PlottingMode;
};

}

#endif // CARTESIANFUNCTION_HPP_INCLUDED
