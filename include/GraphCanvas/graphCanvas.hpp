#ifndef GRAPHCANVAS_HPP_INCLUDED
#define GRAPHCANVAS_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <memory>        //std::unique_ptr
#include <string>        //std::string
#include <unordered_map> //std::unordered_map

//SFML GRAPHICS INCULDES
#include <SFML/Graphics/Drawable.hpp>      //sf::Drawable
#include <SFML/Graphics/Rect.hpp>          //sf::FloatRect
#include <SFML/Graphics/RenderStates.hpp>  //sf::RenderStates
#include <SFML/Graphics/RenderTarget.hpp>  //sf::RenderTarget
#include <SFML/Graphics/RenderTexture.hpp> //sf::RenderTexture
#include <SFML/Graphics/Shader.hpp>        //sf::Shader
#include <SFML/Graphics/Sprite.hpp>        //sf::Sprite
#include <SFML/Graphics/Transformable.hpp> //sf::Transformable
#include <SFML/Graphics/VertexArray.hpp>   //sf::VertexArray
#include <SFML/Graphics/View.hpp>          //sf::View

//INTERNAL INCLUDES
#include "cartesianAxes.hpp"     //gc::CartesianAxes
#include "plotter.hpp"           //gc::Plotter

namespace gc {

class GraphCanvas: public sf::Drawable, public sf::Transformable {
public:
	GraphCanvas ( sf::FloatRect drawArea, sf::FloatRect graphArea );
	void addCartesianAxes ( std::string name, CartesianAxes axes );
	CartesianAxes& getCartesianAxes ( std::string name );
	void removeCartesianAxes ( std::string name );
	void addPlotter ( std::string name, std::unique_ptr<Plotter> plotter );
	Plotter& getPlotter ( std::string name );
	void removePlotter ( std::string name );
	void setDrawArea ( sf::FloatRect drawArea );
	void setGraphArea ( sf::FloatRect graphArea );

	sf::FloatRect getGraphArea() const;

	void preDraw ();
	virtual void draw ( sf::RenderTarget& target, sf::RenderStates states ) const override;
	void update ();
private:
	sf::VertexArray m_VertexArray;

	sf::RenderTexture m_PreFxTexture;
	sf::RenderTexture m_ColourFxTexture[2];
	sf::RenderTexture m_GreyFxTexture[2];
	sf::RenderTexture m_PostFxTexture;
	sf::RenderTexture m_FinalTexture;

	sf::Shader m_ColourShader;
	sf::Shader m_GreyShader;

	sf::Sprite m_PreFxSprite;
	sf::Sprite m_ColourSprite;
	sf::Sprite m_ColourFxSprite;
	sf::Sprite m_GreySprite;
	sf::Sprite m_GreyFxSprite;
	sf::Sprite m_PostFxSprite;

	sf::View m_DefaultView;
	sf::View m_GraphView;

	std::unordered_map<std::string, CartesianAxes> m_Axes;
	std::unordered_map<std::string, std::unique_ptr<Plotter>> m_Plotters;
	std::unordered_map<std::string, double> m_Parameters;
};

}

#endif // GRAPHCANVAS_HPP_INCLUDED
