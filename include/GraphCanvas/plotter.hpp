#ifndef CARTESIANPLOTTER_HPP_INCLUDED
#define CARTESIANPLOTTER_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <functional>    //std::function
#include <memory>        //std::unique_ptr
#include <string>        //std::string
#include <unordered_map> //std::unordered_map

//SFML GRAPHICS INCLUDES
#include <SFML/Graphics/RenderTarget.hpp> //sf::RenderTarget

//INTERNAL INCLUDES
#include "cartesianFunction.hpp" //gc::CartesianFunction::PlottingMode
#include "function.hpp"          //gc::Function
#include "polarFunction.hpp"     //gc::PolarFunction::PlottingMode

namespace gc {

class Plotter {
public:
	Plotter() : m_Var ("x") {}
	Plotter( std::string var );
	Function& getFunction ( std::string name );
	void addCartesianFunction ( std::string name, std::function<double(std::unordered_map<std::string,double>&)> func, CartesianFunction::PlottingMode mode );
	void addPolarFunction ( std::string name, std::function<double(std::unordered_map<std::string,double>&)> func, PolarFunction::PlottingMode mode );
	std::string getIndependentVariable () const;
	void removeFunction ( std::string name );
	void setVariable ( std::string name, double value );
	void setVisibleDomain ( double xStart, double xEnd, double yStart, double yEnd );
	void draw ( sf::RenderTarget& target );
	void update ();
private:
	std::string m_Var;
	std::unordered_map<std::string, std::unique_ptr<Function>> m_Functions;
	std::unordered_map<std::string, double> m_Parameters;

	std::unordered_map<std::string, std::function<void(double,double,double,double)>> m_DomainUpdateQueue;
};

}

#endif // CARTESIANPLOTTER_HPP_INCLUDED
