#ifndef FUNCTION_HPP_INCLUDED
#define FUNCTION_HPP_INCLUDED

//SFML GRAPHICS INCLUDES
#include <SFML/Graphics/Color.hpp>        //sf::Color
#include <SFML/Graphics/RenderTarget.hpp> //sf::RenderTarget
#include <SFML/Graphics/VertexArray.hpp>  //sf::VertexArray

namespace gc {

class Function {
public:
	Function();
	void setColour ( sf::Color colour );
	void setVisibleDomain ( double start, double end );
	void setLowerBound ( double lowerBound );
	void setUpperBound ( double upperBound );
	void removeLowerBound ();
	void removeUpperBound ();
	void setMinimumOut ( double minimumOut );
	void setMaximumOut ( double maximumOut );
	void removeMinimumOut ();
	void removeMaximumOut ();
	void setPointGap ( double gap );
	virtual void draw ( sf::RenderTarget& target ) = 0;
	virtual void update () = 0;
protected:
	void RecreateVertexArray();

	double m_Start;
	double m_End;

	bool m_HasLowerBound;
	double m_LowerBound;

	bool m_HasUpperBound;
	double m_UpperBound;

	double m_Gap;

	bool m_HasMinimumOut;
	double m_MinimumOut;

	bool m_HasMaximumOut;
	double m_MaximumOut;

	sf::Color m_Colour;

	sf::VertexArray m_Vertices;
};

}

#endif // FUNCTION_HPP_INCLUDED
